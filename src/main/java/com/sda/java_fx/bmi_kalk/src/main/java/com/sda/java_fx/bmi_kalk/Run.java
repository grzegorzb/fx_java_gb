package com.sda.java_fx.bmi_kalk;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.List;

import static javafx.application.Application.launch;

public class Run extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Parent paren = FXMLLoader.load(getClass().getResource("/view.fxml"));
        stage.setTitle("BMI index kalkulator");
        stage.setScene(new Scene(paren));
        stage.show();
    }
    public static void main(String[] args) {
        launch();
    }
}
