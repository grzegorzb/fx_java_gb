package com.sda.java_fx.jfx_gb;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Run extends Application {
    // Metoda start to metoda wywołana przy starcie aplikacji JavaFX.
    // Jej argument (typu Stage) to obiekt reprezentujący główne okno aplikacji.
    @Override
    public void start(Stage stage) throws Exception {
//        Parent vBox = FXMLLoader.load(getClass().getResource("/view.fxml"));
        Pane vBox = FXMLLoader.load(getClass().getResource("/view.fxml"));
//         Scena to element głównej części okna. Wypełnimy ten element bazując na naszym widoku.
        stage.setScene(new Scene(vBox));
        stage.setTitle("Apka Grzesia");
        // Pokażemy główne okno aplikacji.
        stage.show();
    }

    public static void main(String[] args) {
        // Metoda launch startuje aplikację JavaFX. Zostanie utworzony obiekt
        // typu rozszerzającego typ javafx.application.Application
        // (w tym wypadku klasy Main).
        launch();
    }
}
