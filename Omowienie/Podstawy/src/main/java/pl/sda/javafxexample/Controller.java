package pl.sda.javafxexample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

// Kontroler to obiekt, który będzie odpowiedzialny za oprogramowanie widoku.
// Przy ładowaniu widoku (FXMLLoader.load), jeśli mamy przypisany widokowi kontroler (korzeń ma atrybut fx:controller),
// to utworzony zostanie kontroler tego typu.
public class Controller {
    // Adnotacja FXML dla pola sprawi, że będzie jemu wstrzyknięta wartość - element widoku o wartości atrybutu
    // fx:id równej nazwie pola. Typ pola i typ elementu widoku muszą być zgodne.
    @FXML
    private Button myButton;
    @FXML
    private CheckBox myCheckBox;
    @FXML
    private Label myLabel;
    @FXML
    private TextField myTextField;

    // W przykładzie będziemy zliczać kliknięcia przycisku.
    private int clickCounter = 0;

    // Jeśli kontroler będzie miał zdefiniowaną metodę o poniższej sygnaturze, to zostanie ona wywołana
    // przy inicjalizacji kontrolera (polom bedą już wstrzyknięte wartości).
    public void initialize() {
        System.out.println("Inicjalizacja kontrolera");

        // Wiele kontrolek posiada właściwość text.
        myLabel.setText("wartość ustawiona przez kontroler");

        // Jeśli chcemy opisać akcję wykonywaną, kiedy przycisk zostanie kliknięty.
        // Użyjemy do tego metody setOnAction przycisku, której jako argument przekażemy za pomocą lambdy
        // instrukcję, co ma się stać, kiedy zostanie kliknięty.
        myButton.setOnAction(event -> {
            // Zwiększymy licznik kliknięć.
            clickCounter++;
            // Wpiszemy nowy tekst w etykietę.
            myLabel.setText("Kliknięto " + clickCounter + " razy");
            // W konsoli wypiszemy informację, czy w momencie kliknięcia przycisku checkbox jest zaznaczony
            System.out.println("Czy checkbox jest zaznaczony: " + myCheckBox.isSelected());
            // oraz treść pola tekstowego.
            System.out.println("Co wpisano w pole tekstowe: " + myTextField.getText());
        });
    }
}
