package pl.sda.javafxexample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
    // Metoda start to metoda wywołana przy starcie aplikacji JavaFX.
    // Jej argument (typu Stage) to obiekt reprezentujący główne okno aplikacji.
    @Override
    public void start(Stage stage) throws Exception {
        // Metoda load klasy FXMLLoader służy do utworzenia obiektów na bazie widoku opisanego
        // za pomocą pliku fxml. My widok zdefiniowaliśmy w pliku src/resources/view.fxml.
        // Metoda zwraca obiekt - kontener, który jest korzeniem widoku.
        VBox vBox = FXMLLoader.load(getClass().getResource("/view.fxml"));
        // Scena to element głównej części okna. Wypełnimy ten element bazując na naszym widoku.
        stage.setScene(new Scene(vBox));
        // Pokażemy główne okno aplikacji.
        stage.show();
    }

    public static void main(String[] args) {
        // Metoda launch startuje aplikację JavaFX. Zostanie utworzony obiekt
        // typu rozszerzającego typ javafx.application.Application
        // (w tym wypadku klasy Main).
        launch();
    }
}
