package pl.sda.listviewexample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        VBox vBox = FXMLLoader.load(getClass().getResource("/view.fxml"));
        stage.setScene(new Scene(vBox, 600, 400));
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
