package pl.sda.listviewexample;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;

public class Controller {
    // Wstrzykniemy polu kontrolera wartość - obiekt typu ListView, który zostanie utworzony przy ładowaniu widoku.
    // Typ ListView jest generyczny - typ sparametryzowany określa, jakiego typu elementy umieszczamy na liście.
    @FXML
    private ListView<String> myListView;

    public void initialize() {
        // Właściwość items to lista elementów kontrolki typu ListView.
        myListView.getItems().addAll("ABC", "XYZ");

        // Właściwość selectionModel to obiekt reprezentujący wszystko związane z zaznaczeniem elementów na liście.
        MultipleSelectionModel<String> selectionModel = myListView.getSelectionModel();
        // Właściwość selectedItem to aktualnie zaznaczony element listy.
        // Każda właściwość posiada opakowanie - umożliwiającą między innymi określenie, co stanie się,
        // gdy wartość zostanie zmieniona.
        // Opiszemy taką akcję dla właściwości selectedItem - powiem co ma się stać, gdy zaznaczony zostanie
        // na liście inny element.
        selectionModel.selectedItemProperty()
                // Metodzie addListener przekazujemy implementację interfejsu ChangeListener.
                // Drugi i trzeci argument implementowanej metody to zaznaczona wartość sprzed zmiany zaznaczenia,
                // oraz zaznaczona wartość po zmianie zaznaczenia.
                .addListener((observableValue, oldValue, newValue) -> {
                    System.out.println("Zmieniono zaznaczenie");
                    System.out.println("Poprzednio zaznaczona wartość: " + oldValue);
                    System.out.println("Nowa zaznaczona wartość: " + newValue);
                });
    }
}
